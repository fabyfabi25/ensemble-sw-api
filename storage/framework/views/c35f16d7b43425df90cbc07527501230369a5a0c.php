
<?php $__env->startSection('content'); ?>
    <div class='row'>
        <div class='col-md-9'>
            <div>
                <h1 class='text-left'><?php echo e($character->name); ?></h1>
                <div class="row">
                    <div class="col-md-6">
                        <?php if($planet): ?>
                            <p class='text-left mb-0'>Home world: <a href="<?php echo e($planet->url); ?>"><?php echo e($planet->name); ?></a></p>
                        <?php endif; ?>
                        <p class='text-left mb-0'>Birth Year: <?php echo e($character->birth_year); ?></p>
                        <p class='text-left mb-0'>Height: <?php echo e($character->height); ?>cm</p>
                        <p class='text-left mb-0'>Mass: <?php echo e($character->mass); ?> KG</p>
                    </div>
                    <div class="col-md-6">
                        <p class='text-left mb-0'>Gender: <?php echo e($character->gender); ?></p>
                        <p class='text-left mb-0'>Skin color: <?php echo e($character->skin_color); ?></p>
                        <p class='text-left mb-0'>Hair color: <?php echo e($character->hair_color); ?></p>
                        <p class='text-left mb-0'>Eye color: <?php echo e($character->eye_color); ?></p>
                        
                    </div>
                </div>
            </div>
            <div class='text-left mt-3'>
                <?php if(!empty($species)): ?>
                    <?php echo $__env->make('components.general', ['title' => 'Species', 'items' => $species], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <?php endif; ?>
                <?php if(!empty($starships)): ?>
                    <?php echo $__env->make('components.general', ['title' => 'Starships', 'items' => $starships], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <?php endif; ?>
                <?php if(!empty($vehicles)): ?>
                    <?php echo $__env->make('components.general', ['title' => 'Vehicles', 'items' => $vehicles], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <?php endif; ?>
            </div>
        </div>
        <div class='col-md-3'>
            <?php echo $__env->make('components.movies', ['movies' => $movies], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\zfaculta-an-4\ensemble\ensemble-mos-fabian\resources\views/layouts/single-page/character-single.blade.php ENDPATH**/ ?>