<div class='mx-auto m-2 w-50'>
    <div class="card">
        <a href="<?php echo e($route); ?>">
            <div class="card-body">
                <h5 class="card-title"><?php echo e($title); ?></h5>
                <h6 class="card-subtitle mb-2 text-muted"><?php echo e($muted); ?></h6>
                <p class="card-text"><?php echo e($excerpt); ?></p>
            </div>
        </a>
    </div>
</div><?php /**PATH C:\zfaculta-an-4\ensemble\ensemble-mos-fabian\resources\views/components/card-list.blade.php ENDPATH**/ ?>