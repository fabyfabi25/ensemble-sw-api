
<?php $__env->startSection('content'); ?>
<div class='row'>
    <div class='col-md-9'>
        <div>
            <h1 class='text-center'><?php echo e($movie->title); ?></h1>
            <span class='row d-flex'>
                <div class='text-center text-muted w-auto mr-auto'>Director: <?php echo e($movie->director); ?></div>
                <div class='text-center text-muted w-auto ml-auto'>Producer: <?php echo e($movie->producer); ?></div>
            </span>
            <h4 class='text-center'>Release date: <?php echo e($movie->release_date); ?></h4>
            <p class='text-center'><?php echo e($movie->opening_crawl); ?></p>

            <div>
                <h3>Planets</h3>
                <ul>
                    <?php $__currentLoopData = $planets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $planet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li>
                            <a href="<?php echo e($planet->url); ?>">
                                <?php echo e($planet->name); ?>

                            </a>
                        </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
            <div>
                <h3>Starships</h3>
                <ul>
                    <?php $__currentLoopData = $starships; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $starship): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li>
                            <a href="<?php echo e($starship->url); ?>">
                                <?php echo e($starship->name); ?>

                            </a>
                        </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
        </div>
    </div>
    <div class='col-md-3'>
        <div>
            <h3>Characters</h3>
            <ul>
                <?php $__currentLoopData = $characters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $character): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li>
                        <a href="<?php echo e($character->url); ?>">
                            <?php echo e($character->name); ?>

                        </a>
                    </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\zfaculta-an-4\ensemble\ensemble-mos-fabian\resources\views/layouts/movie-single.blade.php ENDPATH**/ ?>