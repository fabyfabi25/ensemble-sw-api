<div>
    <h3>Characters</h3>
    <ul class="list-group list-group-flush">
        <?php $__currentLoopData = $characters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $character): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if(isset($character->url)): ?>
                <li class="list-group-item">
                    <a href="<?php echo e($character->url); ?>">
                        <?php echo e($character->name); ?>

                    </a>
                </li>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
</div><?php /**PATH C:\zfaculta-an-4\ensemble\ensemble-mos-fabian\resources\views/components/characters.blade.php ENDPATH**/ ?>