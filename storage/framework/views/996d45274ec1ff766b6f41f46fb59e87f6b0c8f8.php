<header class='masthead mb-5'>
    <div class='inner'>
        <h3 class='masthead-brand'>
            <a href="<?php echo e(route('index')); ?>">Star Wars Api</a>
        </h3>
        <?php
            $movies = route('movies.index');
            $characters = route('characters.index');
            $starships = route('starships.index');
            $vehicles = route('vehicles.index');
            $planets = route('planets.index');
            $species = route('species.index');
            $search = route('search.index');

            $active = request()->is(request()->route()->uri)
        ?>
        
        
        <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link <?php echo e(request()->is('movies') ? 'active' : ''); ?>" href="<?php echo e($movies); ?>">Movies</a>
            <a class="nav-link <?php echo e(request()->is('characters') ? 'active' : ''); ?>" href="<?php echo e($characters); ?>">Characters</a>
            <a class="nav-link <?php echo e(request()->is('starships') ? 'active' : ''); ?>" href="<?php echo e($starships); ?>">Starships</a>
            <a class="nav-link <?php echo e(request()->is('vehicles') ? 'active' : ''); ?>" href="<?php echo e($vehicles); ?>">Vehicles</a>
            <a class="nav-link <?php echo e(request()->is('planets') ? 'active' : ''); ?>" href="<?php echo e($planets); ?>">Planets</a>
            <a class="nav-link <?php echo e(request()->is('species') ? 'active' : ''); ?>" href="<?php echo e($species); ?>">Species</a>
            
        </nav>
    </div>
</header><?php /**PATH C:\zfaculta-an-4\ensemble\ensemble-mos-fabian\resources\views/layouts/navbar.blade.php ENDPATH**/ ?>