<div>
    <h3>Movies</h3>
    <ul class="list-group list-group-flush">
        <?php $__currentLoopData = $movies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $movie): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if(!empty($movie)): ?>
                <li class="list-group-item">
                    <a href="<?php echo e($movie->url); ?>">
                        <?php echo e($movie->title); ?>

                    </a>
                </li>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
</div><?php /**PATH C:\zfaculta-an-4\ensemble\ensemble-mos-fabian\resources\views/components/movies.blade.php ENDPATH**/ ?>