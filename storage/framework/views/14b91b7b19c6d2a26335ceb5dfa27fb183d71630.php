
<?php $__env->startSection('content'); ?>
    <div class='row'>
        <div class='col-md-9'>
            <div>
                <h1 class='text-center'><?php echo e($character->name); ?></h1>
                <span class='row d-flex'>
                    <div class='text-center text-muted w-auto mr-auto'>Gender: <?php echo e($character->gender); ?></div>
                    <div class='text-center text-muted w-auto ml-auto'>Skin color: <?php echo e($character->skin_color); ?></div>
                </span>
                <h4 class='text-center'>Birth Year: <?php echo e($character->birth_year); ?></h4>
                <p class='text-center'>Mass: <?php echo e($character->mass); ?> KG</p>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\zfaculta-an-4\ensemble\ensemble-mos-fabian\resources\views/layouts/character-single.blade.php ENDPATH**/ ?>