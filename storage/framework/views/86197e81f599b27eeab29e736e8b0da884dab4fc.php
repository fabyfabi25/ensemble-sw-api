
<?php $__env->startSection('content'); ?>
    <div class='row'>
        <div class='col-md-9'>
            <div>
                <h1 class='text-center'><?php echo e($starship->name); ?></h1>

                <div class="row mt-5">
                    <div class="col-md-6">
                        <p class='text-center mb-0'>Starship Class: <?php echo e($starship->starship_class); ?></p>
                        <p class='text-center mb-0'>Manufacturer: <?php echo e($starship->manufacturer); ?></p>
                        <p class='text-center mb-0'>Model: <?php echo e($starship->model); ?></p>
                        <p class='text-center mb-0'>Passengers: <?php echo e(number_format((int) $starship->passengers, 0, '.', ',')); ?></p>
                        <p class='text-center mb-0'>Crew: <?php echo e(number_format((int) $starship->crew, 0, '.', ',')); ?></p>
                    </div>
                    <div class="col-md-6">
                        <p class='text-center mb-0'>Cost in credits: <?php echo e(number_format((int) $starship->cost_in_credits, 0, '.', ',')); ?></p>
                        <p class='text-center mb-0'>Cargo capacity: <?php echo e(number_format($starship->cargo_capacity, 0, '.', ',')); ?></p>
                        <p class='text-center mb-0'>MGLT: <?php echo e($starship->MGLT); ?></p>
                        <p class='text-center mb-0'>Consumables: <?php echo e($starship->consumables); ?></p>
                        <p class='text-center mb-0'>Hyperdrive rating: <?php echo e($starship->hyperdrive_rating); ?></p>
                    </div>
                </div>
            </div>
            <div class="text-left mt-3">
                <?php if(!empty($pilots)): ?>
                    <?php echo $__env->make('components.general', ['title' => 'Pilots', 'items' => $pilots], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <?php endif; ?>
            </div>
        </div>
        <div class='col-md-3'>
            <?php echo $__env->make('components.movies', ['movies' => $movies], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\zfaculta-an-4\ensemble\ensemble-mos-fabian\resources\views/layouts/single-page/starship-single.blade.php ENDPATH**/ ?>