<h3 class="mt-3"><?php echo e($title); ?></h3>
<ul class="list-group list-group-flush w-80">
    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if(isset($item->url)): ?>
            <li class="list-group-item">
                <a href="<?php echo e($item->url); ?>">
                    <?php echo e($item->name); ?>

                </a>
            </li>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ul><?php /**PATH C:\zfaculta-an-4\ensemble\ensemble-mos-fabian\resources\views/components/general.blade.php ENDPATH**/ ?>