
<?php $__env->startSection('content'); ?>
    <div class='row'>
        <div class='col-md-9'>
            <div>
                <h1 class='text-center'><?php echo e($vehicle->name); ?></h1>
                <span class='row d-flex'>
                    <div class='text-center text-muted w-auto mr-auto'>Class: <?php echo e($vehicle->vehicle_class); ?></div>
                    <div class='text-center text-muted w-auto ml-auto'>Crew number: <?php echo e($vehicle->crew); ?></div>
                </span>
                <h4 class='text-center'>Model: <?php echo e($vehicle->model); ?></h4>
                <p class='text-center'>vehicle Class: <?php echo e($vehicle->manufacturer); ?></p>
            </div>
        </div>
        <div class='col-md-3'>
            <?php echo $__env->make('components.movies', ['movies' => $movies], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\zfaculta-an-4\ensemble\ensemble-mos-fabian\resources\views/layouts/single-page/vehicle-single.blade.php ENDPATH**/ ?>