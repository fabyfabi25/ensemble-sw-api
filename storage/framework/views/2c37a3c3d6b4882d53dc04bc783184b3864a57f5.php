
<?php $__env->startSection('content'); ?>
    <div class='row'>
        <div class='col-md-9'>
            <div>
                <h1 class='text-center'><?php echo e($specie->name); ?></h1>
                <span class='row d-flex'>
                    <div class='text-center text-muted w-auto mr-auto'><?php echo e($specie->classification); ?></div>
                    <div class='text-center text-muted w-auto ml-auto'>Language: <?php echo e($specie->language); ?></div>
                </span>
                <h4 class='text-center'>Designation: <?php echo e($specie->designation); ?></h4>
                <p class='text-center'>Skin colors: <?php echo e($specie->skin_colors); ?></p>
                <?php if($planet): ?>
                    <a href='<?php echo e($planet->url); ?>'>
                        <p class='text-center'>Home planet: <?php echo e($planet->name); ?></p>
                    </a>
                <?php endif; ?>
            </div>
            <?php echo $__env->make('components.characters', ['characters' => $characters], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
        <div class='col-md-3'>
            <?php echo $__env->make('components.movies', ['movies' => $movies], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\zfaculta-an-4\ensemble\ensemble-mos-fabian\resources\views/layouts/single-page/specie-single.blade.php ENDPATH**/ ?>