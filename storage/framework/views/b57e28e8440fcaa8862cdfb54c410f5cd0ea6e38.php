
<?php $__env->startSection('content'); ?>
    <div class='m-auto'>
        <?php $__currentLoopData = $vehicles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vehicle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="card mx-auto m-2 w-50">
                <div class="card-body">
                    <h5 class="card-title"><?php echo e($vehicle->name); ?></h5>
                    <h6 class="card-subtitle mb-2 text-muted"><?php echo e($vehicle->manufacturer); ?></h6>
                    <p class="card-text"><?php echo e($vehicle->model); ?></p>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\zfaculta-an-4\ensemble\ensemble-mos-fabian\resources\views/layouts/vehicles.blade.php ENDPATH**/ ?>