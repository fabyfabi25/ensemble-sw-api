
<?php $__env->startSection('content'); ?>
    <div class='row'>
        <div class='col-md-9'>
            <div>
                <h1 class='text-center'><?php echo e($planet->name); ?></h1>
               
                <div class="row mt-5">
                    <div class="col-md-6">
                    <p class='text-center mb-0'>Diameter: <?php echo e(number_format($planet->diameter, 0, '.', ',')); ?>km2</p>
                    <p class='text-center mb-0'>Population: <?php echo e($planet->population == 'unknown' ? $planet->population : number_format((int) $planet->population, 0, '.', ',')); ?></p>
                    <p class='text-center mb-0'>Climate: <?php echo e($planet->climate); ?></p>
                    <p class='text-center mb-0'>Terrain: <?php echo e($planet->terrain); ?></p>
                    <p class='text-center mb-0'>Surface water: <?php echo e($planet->surface_water); ?></p>
                    </div>
                    <div class="col-md-6">
                        <p class='text-center mb-0'>Orbital period: <?php echo e($planet->orbital_period); ?> days</p>
                        <p class='text-center mb-0'>Rotation period: <?php echo e($planet->rotation_period); ?> hours</p>
                        <p class='text-center mb-0'>Gravity: <?php echo e($planet->gravity); ?></p>
                    </div>
                </div>
            </div>
            <div class="text-left mt-3">
                <?php if(!empty($characters)): ?>
                    <?php echo $__env->make('components.general', ['title' => 'Characters', 'items' => $characters], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <?php endif; ?>
            </div>
        </div>
        <div class='col-md-3'>
            <?php echo $__env->make('components.movies', ['movies' => $movies], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\zfaculta-an-4\ensemble\ensemble-mos-fabian\resources\views/layouts/single-page/planet-single.blade.php ENDPATH**/ ?>