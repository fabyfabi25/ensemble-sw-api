
<?php $__env->startSection('content'); ?>
    <div class='m-auto'>
        <?php $__currentLoopData = $vehicles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vehicle): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php
                $url = trim(str_replace('https://swapi.dev/api/vehicles/', '', $vehicle->url));
                $route = route('vehicles.single.page', ['url' => $url]);
            ?>
            <?php echo $__env->make('components.card-list', ['title' => $vehicle->name, 'muted' => $vehicle->manufacturer, 'excerpt' => $vehicle->model, 'url' => $url, 'route' => $route], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\zfaculta-an-4\ensemble\ensemble-mos-fabian\resources\views/layouts/page/vehicles.blade.php ENDPATH**/ ?>