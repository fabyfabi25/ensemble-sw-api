
<?php $__env->startSection('content'); ?>
<div class='row'>
    <div class='col-md-9'>
        <div>
            <h1 class='text-center'><?php echo e($movie->title); ?></h1>
            <span class='row d-flex'>
                <div class='text-center text-muted w-auto mr-auto'>Director: <?php echo e($movie->director); ?></div>
                <div class='text-center text-muted w-auto ml-auto'>Producer: <?php echo e($movie->producer); ?></div>
            </span>
            <h4 class='text-center'>Release date: <?php echo e($movie->release_date); ?></h4>
            <p class='text-center'><?php echo e($movie->opening_crawl); ?></p>

            <div class="text-left">
                <?php if(!empty($planets)): ?>
                    <?php echo $__env->make('components.general', ['title' => 'Planets', 'items' => $planets], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <?php endif; ?>
                <?php if(!empty($starships)): ?>
                    <?php echo $__env->make('components.general', ['title' => 'Starships', 'items' => $starships], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class='col-md-3'>
        <?php echo $__env->make('components.characters', ['characters' => $characters], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\zfaculta-an-4\ensemble\ensemble-mos-fabian\resources\views/layouts/single-page/movie-single.blade.php ENDPATH**/ ?>