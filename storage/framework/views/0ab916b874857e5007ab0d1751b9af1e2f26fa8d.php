
<?php $__env->startSection('content'); ?>
    <div class='m-auto'>
        <div class='mb-5 w-50 mx-auto'>
            <form action="<?php echo e(route('movies.index')); ?>">
                <input type="search" name='search' class="form-control" placeholder="Search" aria-label="Search" aria-describedby="search-addon" value="<?php echo e($search ?? ''); ?>"/>
            </form>
        </div>
        <?php $__currentLoopData = $movies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $movie): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php
            $url = trim(str_replace('https://swapi.dev/api/films/', '', $movie->url));
            $route = route('movies.single.page', ['url' => $url]);
        ?>
            <?php echo $__env->make('components.card-list', ['title' => $movie->title, 'muted' => $movie->director, 'excerpt' => $movie->release_date, 'url' => $url, 'route' => $route], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\zfaculta-an-4\ensemble\ensemble-mos-fabian\resources\views/layouts/page.blade.php ENDPATH**/ ?>